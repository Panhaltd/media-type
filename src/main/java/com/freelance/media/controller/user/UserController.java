package com.freelance.media.controller.user;

import com.freelance.media.model.user.User;
import com.freelance.media.model.user.UserRequest;
import com.freelance.media.model.user.UserResponse;
import com.freelance.media.model.user.UserUpdate;
import com.freelance.media.service.UserService;
import com.freelance.media.util.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = ApiConstant.baseUrl + "/users")
public class UserController {

  MethodReducer methodReducer;
  UserService userService;
  Validate validate;
  Message message;

  @Autowired
  public void setMessage(Message message) {
    this.message = message;
  }

  @Autowired
  public void setValidate(Validate validate) {
    this.validate = validate;
  }

  @Autowired
  public void setUserService(UserService userService) {
    this.userService = userService;
  }

  @Autowired
  public void setMethodReducer(MethodReducer methodReducer) {
    this.methodReducer = methodReducer;
  }

  @RequestMapping(method = RequestMethod.GET)
  public ResponseEntity getAllUsers(){
    BaseApiResponse<List<UserResponse>> response = new BaseApiResponse(
            message.success(message.GETSUCCESS, "Users"),
            userService.getAllUsers(),
            HttpStatus.FOUND
    );
    return ResponseEntity.ok(response);
  }

  @GetMapping("/{id}")
  public ResponseEntity getUserById(@PathVariable String id){
    if (validate.checkDigit(id)) {
      UserResponse userResponse = userService.getUserById(Integer.parseInt(id));
      if (userResponse!=null){
        return ResponseEntity.ok(new BaseApiResponse<>(
                message.success(message.GETSUCCESS, "User"),
                userResponse,HttpStatus.FOUND
        ));
      }
      return ResponseEntity.status(404).body(new InvalidApiResponse(
         message.notFound("User",Integer.parseInt(id)),HttpStatus.NOT_FOUND
      ));
    }
    return ResponseEntity.badRequest().body(new InvalidApiResponse(
         message.IDINVALID,HttpStatus.BAD_REQUEST
    ));
  }

  @RequestMapping(method = RequestMethod.POST)
  public ResponseEntity addNewUser(@RequestBody UserRequest user){
    ResponseEntity<InvalidApiResponse> response = checkUser(user);
    if (response == null){
      UserResponse addedUser = userService.addNewUser(user);
      if (addedUser!=null){
        return ResponseEntity.status(HttpStatus.CREATED).body(new BaseApiResponse<>(
                message.success(message.INSERTSUCCESS, "User"),
                addedUser, HttpStatus.CREATED
        ));
      } else {
        return ResponseEntity.internalServerError().body(new InvalidApiResponse(
                message.INTERNALERROR, HttpStatus.INTERNAL_SERVER_ERROR));
      }
    } else{
      return response;
    }
  }

  @PatchMapping("/{id}")
  public ResponseEntity updateUser(@RequestBody UserUpdate user, @PathVariable String id){
    if (validate.checkDigit(id)) {
      UserResponse userResponse = userService.getUserById(Integer.parseInt(id));
      if (userResponse!=null){
        UserResponse updatedUser = userService.updateUser(user,Integer.parseInt(id));
        if (updatedUser!=null){
          return ResponseEntity.status(HttpStatus.CREATED).body(new BaseApiResponse<>(
                  message.success(message.UPDATESUCCESS, "User"),
                  updatedUser, HttpStatus.CREATED
          ));
        } else {
          return ResponseEntity.internalServerError().body(new InvalidApiResponse(
                  message.INTERNALERROR, HttpStatus.INTERNAL_SERVER_ERROR));
        }
      }
      return ResponseEntity.status(404).body(new InvalidApiResponse(
              message.notFound("User",Integer.parseInt(id)),HttpStatus.NOT_FOUND
      ));
    }
    return ResponseEntity.badRequest().body(new InvalidApiResponse(
            message.IDINVALID,HttpStatus.BAD_REQUEST
    ));
  }

  ResponseEntity<InvalidApiResponse> checkUser(UserRequest user){
    if (user.getUserName()==null){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
              message.invalidMessage(message.REQUIRED,"Username"),
              HttpStatus.BAD_REQUEST));
    }
    if (validate.isUsedUsername(user.getUserName())){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
              message.invalidMessage(message.ISUSED,"Username"),
              HttpStatus.BAD_REQUEST));
    }
    if (user.getDisplayName()==null || user.getDisplayName().trim().length()==0){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
              message.invalidMessage(message.REQUIRED,"Display Name"),
              HttpStatus.BAD_REQUEST));
    }
    if (user.getEmail()==null){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
              message.invalidMessage(message.REQUIRED,"Email"),
              HttpStatus.BAD_REQUEST));
    }
    if (!validate.isEmail(user.getEmail())){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
              message.invalidMessage(message.INVALID,"Email"),
              HttpStatus.BAD_REQUEST));
    }
    if (user.getPassword()==null){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
              message.invalidMessage(message.REQUIRED,"Password"),
              HttpStatus.BAD_REQUEST));
    }
    if (!validate.checkPassword(user.getPassword())){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
              message.invalidMessage(message.SHORTPASSWORD,"Password"),
              HttpStatus.BAD_REQUEST));
    }
    if (!validate.checkAlphaNumeric(user.getPassword())){
      return ResponseEntity.badRequest().body(new InvalidApiResponse(
              message.invalidMessage(message.ALPHANUMERIC,"Password"),
              HttpStatus.BAD_REQUEST));
    }
    return null;
  }

}
