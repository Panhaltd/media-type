package com.freelance.media.model.post;

import com.freelance.media.model.user.User;
import com.freelance.media.model.user.UserOther;
import com.freelance.media.model.user.UserResponse;
import org.springframework.expression.spel.ast.Literal;

import java.sql.Timestamp;
import java.util.List;

public class PostResponse {

  private int id;
  private UserOther user;
  private Timestamp date;
  private String title;
  private List<String> images;
  private boolean isEdited;
  private String privacy;

  public PostResponse() {
  }

  public PostResponse(Post post, UserOther user) {
    this.id = post.getId();
    this.user = user;
    this.date = post.getDate();
    this.title = post.getTitle();
    this.isEdited = post.isEdited();
    this.privacy = post.getPrivacy();
  }

  public PostResponse(int id, UserOther user, Timestamp date, String title, List<String> images, boolean isEdited, String privacy) {
    this.id = id;
    this.user = user;
    this.date = date;
    this.title = title;
    this.images = images;
    this.isEdited = isEdited;
    this.privacy = privacy;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public UserOther getUser() {
    return user;
  }

  public void setUser(UserOther user) {
    this.user = user;
  }

  public Timestamp getDate() {
    return date;
  }

  public void setDate(Timestamp date) {
    this.date = date;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<String> getImages() {
    return images;
  }

  public void setImages(List<String> images) {
    this.images = images;
  }

  public boolean isEdited() {
    return isEdited;
  }

  public void setEdited(boolean edited) {
    isEdited = edited;
  }

  public String getPrivacy() {
    return privacy;
  }

  public void setPrivacy(String privacy) {
    this.privacy = privacy;
  }

  @Override
  public String toString() {
    return "PostResponse{" +
            "id=" + id +
            ", user=" + user +
            ", date=" + date +
            ", title='" + title + '\'' +
            ", images=" + images +
            ", isEdited=" + isEdited +
            ", privacy='" + privacy + '\'' +
            '}';
  }
}
