package com.freelance.media.model.post;

import java.sql.Timestamp;
import java.util.List;

public class PostRequest {

  private String userId;
  private String title;
  private List<String> images;
  private String privacy;

  public PostRequest() {
  }

  public PostRequest(String userId, String title, List<String> images, String privacy) {
    this.userId = userId;
    this.title = title;
    this.images = images;
    this.privacy = privacy;
  }

  public String getUserId() {
    return userId;
  }

  public void setUserId(String userId) {
    this.userId = userId;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public List<String> getImages() {
    return images;
  }

  public void setImages(List<String> images) {
    this.images = images;
  }

  public String getPrivacy() {
    return privacy;
  }

  public void setPrivacy(String privacy) {
    this.privacy = privacy;
  }

  @Override
  public String toString() {
    return "PostRequest{" +
            "userId=" + userId +
            ", title='" + title + '\'' +
            ", images=" + images +
            ", privacy='" + privacy + '\'' +
            '}';
  }
}
