package com.freelance.media.model.user;

public class UserOther {
  private int id;
  private String userName;
  private String displayName;
  private String imagePath;

  public UserOther() {
  }

  public UserOther(UserResponse user){
    this.id = user.getId();
    this.userName = user.getUserName();
    this.displayName = user.getDisplayName();
    this.imagePath = user.getImagePath();
  }

  public UserOther(int id, String userName, String displayName, String imagePath) {
    this.id = id;
    this.userName = userName;
    this.displayName = displayName;
    this.imagePath = imagePath;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  @Override
  public String toString() {
    return "UserOther{" +
            "id=" + id +
            ", userName='" + userName + '\'' +
            ", displayName='" + displayName + '\'' +
            ", imagePath='" + imagePath + '\'' +
            '}';
  }
}
