package com.freelance.media.model.user;

import java.sql.Date;
import java.sql.Timestamp;

public class UserResponse {
  private int id;
  private String userName;
  private String displayName;
  private String email;
  private String gender;
  private String address;
  private String phoneNumber;
  private String imagePath;
  private String token;
  private Date dob;
  private Timestamp createdDate;
  private boolean status;

  public UserResponse() {
  }

  public UserResponse(User user){
    this.id = user.getId();
    this.userName = user.getUserName();
    this.displayName = user.getDisplayName();
    this.email = user.getEmail();
    this.gender = user.getGender();
    this.address = user.getAddress();
    this.phoneNumber = user.getPhoneNumber();
    this.imagePath = user.getImagePath();
    this.token = user.getToken();
    this.dob = user.getDob();
    this.createdDate = user.getCreatedDate();
    this.status = user.isStatus();
  }

  public UserResponse(int id, String userName, String displayName, String email, String gender, String address, String phoneNumber, String imagePath, String token, Date dob, Timestamp createdDate, boolean status) {
    this.id = id;
    this.userName = userName;
    this.displayName = displayName;
    this.email = email;
    this.gender = gender;
    this.address = address;
    this.phoneNumber = phoneNumber;
    this.imagePath = imagePath;
    this.token = token;
    this.dob = dob;
    this.createdDate = createdDate;
    this.status = status;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Date getDob() {
    return dob;
  }

  public void setDob(Date dob) {
    this.dob = dob;
  }

  public Timestamp getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Timestamp createdDate) {
    this.createdDate = createdDate;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "UserResponse{" +
            "id=" + id +
            ", userName='" + userName + '\'' +
            ", displayName='" + displayName + '\'' +
            ", email='" + email + '\'' +
            ", gender='" + gender + '\'' +
            ", address='" + address + '\'' +
            ", phoneNumber='" + phoneNumber + '\'' +
            ", imagePath='" + imagePath + '\'' +
            ", token='" + token + '\'' +
            ", dob=" + dob +
            ", createdDate=" + createdDate +
            ", status=" + status +
            '}';
  }
}
