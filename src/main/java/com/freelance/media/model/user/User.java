package com.freelance.media.model.user;

import com.freelance.media.util.ApiConstant;

import java.sql.Date;
import java.sql.Timestamp;

public class User {
  private int id;
  private String userName;
  private String displayName;
  private String email;
  private String password;
  private String gender;
  private String address;
  private String phoneNumber;
  private String imagePath;
  private String token;
  private Date dob;
  private Timestamp createdDate;
  private boolean status;

  public User() {
  }

  public User(UserRequest user){
    this.userName = user.getUserName().trim();
    this.displayName = user.getDisplayName().trim().replaceAll("\\s+", " ");
    this.email = user.getEmail();
    this.gender = user.getGender();
    if (user.getAddress()!=null)
    this.address = user.getAddress().trim().replaceAll("\\s+", " ");
    this.password = user.getPassword();
    this.phoneNumber = user.getPhoneNumber();
    if (user.getImagePath()==null){
      this.imagePath = ApiConstant.placeHolder;
    } else {
      this.imagePath = user.getImagePath();
    }
    this.dob = user.getDob();
  }

  public User(UserUpdate user){
    if (user.getDisplayName()!=null)
    this.displayName = user.getDisplayName().trim().replaceAll("\\s+", " ");
    this.gender = user.getGender();
    if (user.getAddress()!=null)
    this.address = user.getAddress().trim().replaceAll("\\s+", " ");
    this.phoneNumber = user.getPhoneNumber();
    this.imagePath = user.getImagePath();
    this.dob = user.getDob();
  }

  public User(int id, String userName, String displayName, String email, String password, String gender, String address, String phoneNumber, String imagePath, String token, Date dob, Timestamp createdDate, boolean status) {
    this.id = id;
    this.userName = userName;
    this.displayName = displayName;
    this.email = email;
    this.password = password;
    this.gender = gender;
    this.address = address;
    this.phoneNumber = phoneNumber;
    this.imagePath = imagePath;
    this.token = token;
    this.dob = dob;
    this.createdDate = createdDate;
    this.status = status;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUserName() {
    return userName;
  }

  public void setUserName(String userName) {
    this.userName = userName;
  }

  public String getDisplayName() {
    return displayName;
  }

  public void setDisplayName(String displayName) {
    this.displayName = displayName;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getPhoneNumber() {
    return phoneNumber;
  }

  public void setPhoneNumber(String phoneNumber) {
    this.phoneNumber = phoneNumber;
  }

  public String getImagePath() {
    return imagePath;
  }

  public void setImagePath(String imagePath) {
    this.imagePath = imagePath;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String token) {
    this.token = token;
  }

  public Date getDob() {
    return dob;
  }

  public void setDob(Date dob) {
    this.dob = dob;
  }

  public Timestamp getCreatedDate() {
    return createdDate;
  }

  public void setCreatedDate(Timestamp createdDate) {
    this.createdDate = createdDate;
  }

  public boolean isStatus() {
    return status;
  }

  public void setStatus(boolean status) {
    this.status = status;
  }

  @Override
  public String toString() {
    return "User{" +
            "id=" + id +
            ", userName='" + userName + '\'' +
            ", displayName='" + displayName + '\'' +
            ", email='" + email + '\'' +
            ", password='" + password + '\'' +
            ", gender='" + gender + '\'' +
            ", address='" + address + '\'' +
            ", phoneNumber='" + phoneNumber + '\'' +
            ", imagePath='" + imagePath + '\'' +
            ", token='" + token + '\'' +
            ", dob=" + dob +
            ", createdDate=" + createdDate +
            ", status=" + status +
            '}';
  }
}
