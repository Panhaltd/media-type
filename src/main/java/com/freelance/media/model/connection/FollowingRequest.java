package com.freelance.media.model.connection;

public class FollowingRequest {

  private String follower;
  private String user;

  public FollowingRequest() {
  }

  public FollowingRequest(String follower, String user) {
    this.follower = follower;
    this.user = user;
  }

  public String getFollower() {
    return follower;
  }

  public void setFollower(String follower) {
    this.follower = follower;
  }

  public String getUser() {
    return user;
  }

  public void setUser(String user) {
    this.user = user;
  }

  @Override
  public String toString() {
    return "FollowingRequest{" +
            "follower='" + follower + '\'' +
            ", user='" + user + '\'' +
            '}';
  }
}
