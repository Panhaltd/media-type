package com.freelance.media.util;

public class ApiConstant {
  public final static String baseUrl = "/api/v1";
  public final static String placeHolder = "C://asus/home/data/image.png";
  public final static String PUBLIC = "public";
  public final static String FRIEND = "friend";
}
