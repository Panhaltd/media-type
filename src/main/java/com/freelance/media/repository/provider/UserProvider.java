package com.freelance.media.repository.provider;

import com.freelance.media.model.user.User;
import org.apache.ibatis.jdbc.SQL;

public class UserProvider {

  public String getAllUsers(){
    return new SQL(){{
      SELECT("*");
      FROM("md_user");
    }}.toString();
  }

  public String getUserById(){
    return new SQL(){{
      SELECT("*");
      FROM("md_user");
      WHERE("id=#{id}");
    }}.toString();
  }

  public String isUsedUsername(String username){
    return new SQL(){{
      SELECT("*");
      FROM("md_user");
      WHERE("user_name=#{username}");
    }}.toString();
  }

  public String addNewUser(){
    return new SQL(){{
      INSERT_INTO("md_user");
      VALUES("user_name","#{userName}");
      VALUES("display_name","#{displayName}");
      VALUES("password","#{password}");
      VALUES("email","#{email}");
      VALUES("gender","#{gender}");
      VALUES("dob","#{dob}");
      VALUES("phone_number","#{phoneNumber}");
      VALUES("address","#{address}");
      VALUES("token","#{token}");
      VALUES("image_path","#{imagePath}");
      VALUES("created_date","#{createdDate}");
      VALUES("status","#{status}");
    }}.toString();
  }

  public String updateUser(User user, int id){
    return new SQL(){{
      UPDATE("md_user");
      if (user.getDisplayName()!=null)
        SET("display_name=#{user.displayName}");
      if (user.getGender()!=null)
        SET("gender=#{user.gender}");
      if (user.getDob()!=null)
        SET("dob=#{user.dob}");
      if (user.getPhoneNumber()!=null)
        SET("phone_number=#{user.phoneNumber}");
      if (user.getAddress()!=null)
        SET("address=#{user.address}");
      if (user.getImagePath()!=null)
        SET("image_path=#{user.imagePath}");
      WHERE("id=#{id}");
    }}.toString();
  }
}
