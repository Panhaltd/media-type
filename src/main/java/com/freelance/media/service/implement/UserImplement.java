package com.freelance.media.service.implement;

import com.freelance.media.model.user.User;
import com.freelance.media.model.user.UserRequest;
import com.freelance.media.model.user.UserResponse;
import com.freelance.media.model.user.UserUpdate;
import com.freelance.media.repository.UserRepository;
import com.freelance.media.service.UserService;
import com.freelance.media.util.MethodReducer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserImplement implements UserService {

  UserRepository userRepository;
  MethodReducer methodReducer;

  @Autowired
  public void setMethodReducer(MethodReducer methodReducer) {
    this.methodReducer = methodReducer;
  }

  @Autowired
  public void setUserRepository(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  @Override
  public List<UserResponse> getAllUsers() {
    List<UserResponse> list = new ArrayList<>();
    for (User user: userRepository.getAllUser()) {
      list.add(new UserResponse(user));
    }
    return list;
  }

  @Override
  public UserResponse getUserById(int id) {
    User user = userRepository.getUserById(id);
    if (user!=null){
      return new UserResponse(user);
    }
    return null;
  }

  @Override
  public UserResponse addNewUser(UserRequest newUser) {
    User user = new User(newUser);
    user.setPassword(methodReducer.encryptPassword(user.getPassword()));
    user.setToken(methodReducer.encryptPassword(user.getPassword()));
    user.setCreatedDate(methodReducer.getTime());
    user.setStatus(true);
    boolean isAdded;
    isAdded = userRepository.addNewUser(user);
    if (isAdded){
      user.setId(user.getId());
      return new UserResponse(user);
    }
    return null;
  }

  @Override
  public UserResponse updateUser(UserUpdate updateUser, int id) {
    User user = new User(updateUser);
    boolean isUpdated;
    isUpdated = userRepository.updateUser(user,id);
    if (isUpdated){
      return getUserById(id);
    }
    return null;
  }

  public boolean isUsedUsername(String username){
    return userRepository.isUsedUsername(username).size() > 0;
  }

}
